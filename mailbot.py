#! /usr/bin/python3

from email.mime.text import MIMEText
from flask import Flask, abort, request
import json
import smtplib
import textwrap
from threading import Thread
app = Flask(__name__)

def send_mail(msg):
    with smtplib.SMTP('my-server-name') as s:
        s.send_message(msg)
        s.quit()

@app.route('/webhook/new-mr', methods=['POST'])
def new_mr_hook():
    payload = request.get_json(force=True)
    if payload['object_kind'] != 'merge_request':
        abort(403)
    if payload['event_type'] != 'merge_request':
        abort(403)

    # We only care about new MRs
    changes = payload['changes']
    if 'id' not in changes or changes['id']['previous'] is not None:
        return 'OK'

    mr = payload['object_attributes']
    if mr['target_branch'] != 'master':
        return 'OK'

    subject = '[MERGE REQUEST #{}] {}'.format(mr['iid'], mr['title'])
    body = textwrap.dedent("""\
        From: {} (@{})

        {}

        This merge request can be found at:

            {}
     """).format(payload['user']['name'], payload['user']['username'],
                 mr['description'], mr['url'])

    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = 'gitlab-bot@freedesktop.org'
    msg['To'] = 'mesa-dev@lists.freedesktop.org'

    # Kick off the email send in a thread so we don't block and have the
    # GitLab server time out on us
    thread = Thread(target=send_mail, args=(msg,))
    thread.start()

    return 'OK'
